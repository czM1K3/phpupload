// Úspěšně vykradeno z: https://stackoverflow.com/questions/8006715/drag-drop-files-into-standard-html-file-input
const dropContainer = document.querySelector("#dropContainer");
const fileInput = document.querySelector("#file");

dropContainer.ondragover = dropContainer.ondragenter = function (evt){
    evt.preventDefault();
}

dropContainer.ondrop = function (evt){
    fileInput.files = evt.dataTransfer.files;
    evt.preventDefault();
}