<?php
//require_once "vendor/autoload.php";

//use Tracy\Debugger;

//Debugger::enable();
?>
<!doctype html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Upload souboru</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body id="dropContainer" style="min-height: 100vh">
<div class="container">
    <h1>Upload souboru</h1>

    <?php

    if ($_FILES) {
        $targetDir = "uploads/";
        $targetFile = $targetDir . basename($_FILES['uploadedName']['name']);
        $fileType = explode("/", $_FILES['uploadedName']['type'])[0];

        $uploadSuccess = true;
        //dump($_FILES);

        if ($_FILES['uploadedName']['error'] != 0) {
            echo "<p class='text-danger'>Chyba serveru při uploadu</p>";
            $uploadSuccess = false;

        } //kontrola existence
        elseif (file_exists($targetFile)) {
            echo "<p class='text-danger'>Soubor již existuje</p>";
            $uploadSuccess = false;
        } //kontrola velikosti
        elseif ($_FILES['uploadedName']['size'] > 8388608) {
            echo "<p class='text-danger'>Soubor je příliš velký</p>";
            $uploadSuccess = false;
        } //kontrola typu
        elseif ($fileType !== "image" && $fileType !== "video" && $fileType !== "audio") {
            echo "<p class='text-danger'>Soubor má špatný typ</p>";
            $uploadSuccess = false;
        }
        if (!$uploadSuccess) {
            echo "<p class='text-danger'>Došlo k chybě uploadu</p>";
        } else {
            //vše je OK
            //přesun souboru
            if (move_uploaded_file($_FILES['uploadedName']['tmp_name'], $targetFile)) {
                chmod($targetFile, 0755);
                header("Location: uploads/" . basename($_FILES['uploadedName']['name']));
            } else {
                echo "<p class='text-danger'>Došlo k chybě uploadu</p>";
            }
        }

    }

    ?>
    <form method='post' action='' enctype='multipart/form-data'>
        <div class="mb-3">
            <label for="file" class="form-label">Vyberte soubor k nahrání:</label>
            <input id="file" class="form-control" type="file" name="uploadedName" accept="image/*, video/*, audio/*"/>
            <input type="submit" value="Nahrát" name="submit" class="btn btn-primary m-2"/>
        </div>
    </form>
</div>
<script src="script.js"></script>
</body>
</html>
